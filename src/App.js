import React, { Component } from 'react';
import ReactTable from 'react-table';
import './App.css';
import 'react-table/react-table.css';

var firebase = require('firebase/app');

require('firebase/database');

function deleteOldStatusLogs(info) {
	firebase.database().ref('/logs/').once('value', snapshot => {
		var promises = [];
		snapshot.forEach(function(child) {
			const entry = snapshot.val()[child.key];
			if (entry.type === 'info' && entry.date !== info.date) {
				promises.push(child.ref.remove());
			}
		});
		Promise.all(promises);
	})
}

function sortByKey(items, key) {
	return items.sort((a, b) => {
		const aFloat = parseFloat(a[key]);
		const bFloat = parseFloat(b[key]);

		return (aFloat > bFloat) ? -1 : ((bFloat > aFloat) ? 1 : 0);
	});
}

function formatedDateFromTimeStamp(timestamp) {
	const date = new Date(timestamp);
	const dateOptions = {
		year: 'numeric',
		month: '2-digit',
		day: '2-digit',
		hour: '2-digit',
		minute: '2-digit',
		second: '2-digit'
	};
	return date.toLocaleString('de-DE', dateOptions)
}

function getTradesTotal(values) {
	let total = 0;

	Object.keys(values).forEach(key => {
		const value = values[key];
		if (value.type === 'sell') {
			const amount = parseInt(values[key].amountDollar, 10);
			total += amount;
		}
	});
	return total;
}

function getMonthsSummary(values) {

	const monthsObject = {};
	const months = [];
	const monthNames = [
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'July',
		'August',
		'September',
		'October',
		'November'
	];

	Object.keys(values).forEach(key => {
		const value = values[key];

		if (value.type !== 'sell') {
			return;
		}

		// create an object with year+month as keys and an array of trades
		// in that perios
		const date = new Date(value.date);
		const yearMonth = date.getFullYear() + ' ' + monthNames[date.getMonth()];

		if (monthsObject[yearMonth]) {
			monthsObject[yearMonth].push(value);
		} else {
			monthsObject[yearMonth] = [value];
		}

	});

	// create the result array out of monthsObject
	Object.keys(monthsObject).forEach(key => {
		const value = monthsObject[key];
		let trades = 0;
		let amount = 0;

		value.forEach(item => {
			trades++;
			amount += item.amountDollar;
		});

		months.push({
			month: key,
			trades: trades,
			profit: parseInt(amount, 10)
		});
	});


	// month, trades, profit
	return months;
}

class App extends Component {

	transformValues(values) {
		const trades = [];
		const info = [];
		const errors = [];

		if (!values) {
			return;
		}

		Object.keys(values).forEach(function (key) {
			const dbValue = values[key];

			if (dbValue.type === 'info') {
				info.push(dbValue);
			} else if (dbValue.type === 'error') {
				errors.push(dbValue);
			} else if (dbValue.type === 'buy' || dbValue.type === 'sell') {
				trades.push(dbValue);
			}
		});

		// get last update date
		const sortedItems = sortByKey(info, 'date');
		const lastInfo = sortedItems[0];
		const lastDateFormated = formatedDateFromTimeStamp(lastInfo.date);
		deleteOldStatusLogs(lastInfo);

		// get errors
		const sortedErrors = sortByKey(errors, 'date');
		const preparedErrors = sortedErrors.map(item => {
			item.dateDisplay = formatedDateFromTimeStamp(item.date);
			return item;
		});

		// get trades
		const sortedTrades = sortByKey(trades, 'date');
		const preparedTrades = sortedTrades.map(item => {
			item.dateDisplay = formatedDateFromTimeStamp(item.date);
			return item;
		});

		// get total of trades
		const tradesTotal = getTradesTotal(values);

		// get months summary
		const monthsSummary = getMonthsSummary(sortedTrades);

		this.setState({
			update: lastDateFormated,
			errors: preparedErrors,
			trades: preparedTrades,
			summary: {
				total: tradesTotal,
				months: monthsSummary
			}
		})
	}

	constructor(props) {
		super(props);

		this.state = {
			update: [],
			errors: [],
			trades: [],
			summary: {
				total: 0,
				months: []
			}
		}

		var config = {
			apiKey: 'AIzaSyC8XtaPiSdDHbKwjnxzrIU5gRBPTlqUPG8',
			authDomain: 'binance-60ecf.firebaseapp.com',
			databaseURL: 'https://binance-60ecf.firebaseio.com',
			projectId: 'binance-60ecf',
			storageBucket: 'binance-60ecf.appspot.com',
			messagingSenderId: '106325763722'
		};

		firebase.initializeApp(config);

		var database = firebase.database();

		database.ref('/logs/').once('value').then(function(snapshot) {
			this.transformValues(snapshot.val());
		}.bind(this));

		this.columns = [
			{
				Header: 'Date',
				accessor: 'dateDisplay',
				show: true,
				width: 120,
				Cell: (row) => (<span className='colDate'>{row.value}</span>)
			},
			{
				Header: 'Type',
				accessor: 'type',
				show: true,
				width: 80,
				Cell: (row) => {
					return (
						<div className={'colType ' + (row.value === 'sell' ? 'green' : 'red')}>
							{row.value}
						</div>
					);
				}
			},
			{
				Header: 'Asset',
				id: 'asset',
				width: 80,
				accessor: dataItem => {
					if (dataItem.type === 'sell') {
						return dataItem.asset;
					} else {
						const split = dataItem.asset.split(dataItem.baseAsset);
						return split[0];
					}
				},
				Cell: (row) => (<span className='colAsset'>{row.value}</span>),
				show: true
			},
			{
				Header: 'Amount',
				id: 'amount',
				accessor: dataItem => (dataItem.amount.toFixed(2)),
				show: true,
				width: 100,
				Cell: (row) => (<span className='colAmount'>{row.value}</span>)
			},
			{
				Header: 'Amount $',
				id: 'amountDollar',
				accessor: dataItem => (dataItem.amountDollar.toFixed(2)),
				show: true,
				Cell: (row) => (<span className='colAmountDollar'>{row.value}</span>)
			},
			{
				Header: 'Price $',
				id: 'priceDollar',
				accessor: dataItem => (dataItem.priceDollar.toFixed(4)),
				show: true,
				Cell: (row) => (<span className='colPrice'>{row.value}</span>)
			}
		];

		this.columnsSummary = [
			{
				Header: 'Month',
				accessor: 'month',
				show: true,
				width: 120,
				Cell: (row) => (<span className='colSummaryMonths'>{row.value}</span>)
			},
			{
				Header: 'Trades',
				accessor: 'trades',
				show: true,
				width: 120,
				Cell: (row) => (<span className='colSummaryTrades'>{row.value}</span>)
			},
			{
				Header: 'Profit',
				accessor: 'profit',
				show: true,
				width: 120,
				Cell: (row) => (<span className='colSummaryProfit'>{row.value}</span>)
			}
		];

		this.columnsErrors = [
			{
				Header: 'Date',
				accessor: 'dateDisplay',
				show: true,
				width: 120,
				Cell: (row) => (<span className='colDate'>{row.value}</span>)
			},
			{
				Header: 'Text',
				accessor: 'text',
				show: true
			}
		];
	}

	render() {
		return (
			<div className="App">

				<div className="update">
					<span className="update_description">
						Last update:
					</span>
					<span className="update_value">
						{this.state.update}
					</span>
				</div>

				<h1 className="title">Trades<span className="subtitle">{this.state.trades.length}</span></h1>

				<ReactTable
					data={this.state.trades}
					columns={this.columns}
					defaultPageSize={10}
					className='-striped -highlight'
					showPageSizeOptions={false}
					showPageJump={false}
					resizable={false}
					pageText=''
					ofText='/'
				/>

			<h1 className="title">Summary<span className="subtitle">{this.state.summary.total} $</span></h1>

				<ReactTable
					data={this.state.summary.months}
					columns={this.columnsSummary}
					defaultPageSize={10}
					className='-striped -highlight'
					showPageSizeOptions={false}
					showPageJump={false}
					resizable={false}
					pageText=''
					ofText='/'
				/>





				<h1 className="title">Errors<span className="subtitle">{this.state.errors.length}</span></h1>

				<ReactTable
					data={this.state.errors}
					columns={this.columnsErrors}
					defaultPageSize={10}
					className='-striped -highlight'
					showPageSizeOptions={false}
					showPageJump={false}
					resizable={false}
					pageText=''
					ofText='/'
				/>

			</div>
		);
	}
}

export default App;
